class RegisterExpression:
    def __init__(self):
        self.topsymbol = 0
    def simplify(self):
        return self
    def getText(self):
        return ""

def smaller(x:RegisterExpression,y:RegisterExpression):
    return (x.topsymbol < y.topsymbol) | ((x.topsymbol == y.topsymbol) & (x.getText() <= y.getText()))

def equal(x:RegisterExpression,y:RegisterExpression):
    return smaller(x,y) & smaller(y,x)
    
class Register(RegisterExpression):
    def __init__(self, name = ""):
        self.name = name
        self.topsymbol = 1
    def getText(self):
        return self.name

class ALUcc(RegisterExpression):
    def __init__(self, A:RegisterExpression):
        self.A = A
        self.topsymbol = 2
    def getText(self):
        return "ALU.cc[" + self.A.getText() + "]"
    def simplify(self):
        return ALUcc(self.A.simplify())
    
class RAM(RegisterExpression):
    def __init__(self, address:RegisterExpression):
        self.address = address
        self.topsymbol = 3
    def getText(self):
        return "RAM[" + self.address.getText() + "]"
    def simplify(self):
        return RAM(self.address.simplify())

class IncRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression):
        self.A = A
        self.topsymbol = 4
    def getText(self):
        return "(" + self.A.getText() + "+1)"
    def simplify(self):
        x = self.A.simplify()
        if type(x) == DecRegister:
            return x.A
        else:
            return IncRegister(x)

class DecRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression):
        self.A = A
        self.topsymbol = 5
    def getText(self):
        return "(" + self.A.getText() + "-1)"
    def simplify(self):
        x = self.A.simplify()
        if type(x) == IncRegister:
            return x.A
        else:
            return DecRegister(x)
        
class CompRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression, B:RegisterExpression):
        self.A = A
        self.B = B
        self.topsymbol = 6
    def getText(self):
        return "(" + self.A.getText() + "!=" + self.B.getText() + ")"
    def simplify(self):
        x = self.A.simplify()
        y = self.B.simplify()
        if smaller(x,y):
            return CompRegister(x,y)
        else:
            return CompRegister(y,x)

class AndRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression, B:RegisterExpression):
        self.A = A
        self.B = B
        self.topsymbol = 7
    def getText(self):
        return "(" + self.A.getText() + "&&" + self.B.getText() + ")"
    def simplify(self):
        x = self.A.simplify()
        y = self.B.simplify()        
        if type(x) is AndRegister:
            z = AndRegister(x.A,AndRegister(x.B,y))        
            return z.simplify()
        elif type(y) is AndRegister:
            if smaller(x,y.A):
                if smaller(y.A,x):
                    z = AndRegister(x,y.B)
                    return z.simplify()
                else:
                    return AndRegister(x,y)
            else:
                z = AndRegister(y,x)
                return z.simplify()       
        elif smaller(x,y):
            if smaller(y,x):
                return x
            else:
                return AndRegister(x,y)
        else:
            z = AndRegister(y,x)
            return z.simplify()

class OrRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression, B:RegisterExpression):
        self.A = A
        self.B = B
        self.topsymbol = 8
    def getText(self):
        return "(" + self.A.getText() + "||" + self.B.getText() + ")"
    def simplify(self):
        x = self.A.simplify()
        y = self.B.simplify()
        if type(x) is OrRegister:
            z = OrRegister(x.A,OrRegister(x.B,y))
            return z.simplify()
        elif type(x) is AndRegister:
            z = AndRegister(OrRegister(x.A,y),OrRegister(x.B,y))
            return z.simplify()        
        elif (type(y) is OrRegister):
            if smaller(x,y.A):
                if smaller(y.A,x):
                    z = OrRegister(x,y.B)
                    return z.simplify()
                else:
                    return OrRegister(x,y)
            else:
                z = OrRegister(y,x)
                return z.simplify()
        elif type(y) is AndRegister:
            z = AndRegister(OrRegister(x,y.A),OrRegister(x,y.B))
            return z.simplify()
        elif smaller(x,y):
            if smaller(y,x):
                return x
            else:
                return OrRegister(x,y)
        else:
            z = OrRegister(y,x)
            return z.simplify()

class AddRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression, B:RegisterExpression):
        self.A = A
        self.B = B
        self.topsymbol = 9
    def getText(self):
        return "(" + self.A.getText() + "+" + self.B.getText() + ")"
    def simplify(self):
        x = self.A.simplify()
        y = self.B.simplify()
        if type(x) is IncRegister:
            z = IncRegister(AddRegister(x.A,y))
            return z.simplify()
        elif type(x) is DecRegister:
            z = DecRegister(AddRegister(x.A,y))
            return z.simplify()
        elif type(y) is IncRegister:
            z = IncRegister(AddRegister(x,y.A))
            return z.simplify()
        elif type(y) is DecRegister:
            z = DecRegister(AddRegister(x,y.Y))
            return z.simplify()
        elif type(x) is AddRegister:
            z = AddRegister(x.A,AddRegister(x.B,y))
            return z.simplify()        
        elif (type(y) is AddRegister):
            if smaller(x,y.A):
                return AddRegister(x,y)
            else:
                z = AddRegister(y,x)
                return z.simplify()
        elif smaller(x,y):
            return AddRegister(x,y)
        else:
            z = AddRegister(y,x)
            return z.simplify()    
    
class SubRegister(RegisterExpression):
    def __init__(self, A:RegisterExpression, B:RegisterExpression):
        self.A = A
        self.B = B
        self.topsymbol = 10
    def getText(self):
        return "(" + self.A.getText() + "-" + self.B.getText() + ")"
    def simplify(self):
        x = self.A.simplify()
        y = self.B.simplify()
        if type(x) is IncRegister:
            z = IncRegister(SubRegister(x.A,y))
            return z.simplify()
        elif type(x) is DecRegister:
            z = DecRegister(SubRegister(x.A,y))
            return z.simplify()
        elif type(y) is IncRegister:
            z = DecRegister(SubRegister(x,y.A))
            return z.simplify()
        elif type(y) is DecRegister:
            z = IncRegister(SubRegister(x,y.Y))
            return z.simplify()
        elif type(x) is AddRegister:
            z = SubRegister(x.A,SubRegister(y,x.B))
            return z.simplify()
        elif type(x) is SubRegister:
            z = SubRegister(x.A,AddRegister(y,x.B))
            return z.simplify()
        else:
            return SubRegister(x,y)

class DontCare(RegisterExpression):
    def __init__(self, name = ""):
        self.name = name
        self.topsymbol = 11
