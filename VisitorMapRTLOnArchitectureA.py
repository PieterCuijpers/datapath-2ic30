# This visitor verifies whether a given register transfer language program is unambiguous for execution
# on the basic architecture. This means that the use of certain registers is not allowed, and that
# the use of certain registers requires the declaration of a bus.
#

from RTLParser import RTLParser
from RTLVisitor import RTLVisitor
from RegisterExpression import *

class CheckVisitorA(RTLVisitor):
 
    def __init__(self):
        self.state = True
        self.linenumber = 0
        self.bus = []
        pass
    
    def visitProgram(self, ctx:RTLParser.ProgramContext):
        self.visitChildren(ctx)
        return self.state

    def visitRegisterTransfer(self, ctx:RTLParser.LineContext):
        #First create two lists, the leftlist contains target registers
        #the rightlist contains RegisterExpressions representing source registers
        self.linenumber = self.linenumber + 1
        self.bus = []
        leftlist = self.visit(ctx.registerListLHS())
        rightlist = self.visit(ctx.registerListRHS())

        #Then check each individual transfer (in this case, just print it)...
        for i in range(len(leftlist)):
            #check if implicit bus-usage is correct
            if leftlist[i].getText() == 'SigmaW' and not rightlist[i].getText() in ['RA:Abus','RB:Abus','IP:Abus']:
                print('Line ' + str(self.linenumber) + ': assignment to SigmaW requires explict use of Abus')
                self.state = False
            if rightlist[i].getText() == 'RAM[SigmaA]' and not leftlist[i].getText() in ['SigmaR']:
                print('Line ' + str(self.linenumber) + ': RAM[SigmaA] can only be assigned to SigmaR')
                self.state = False
            if rightlist[i].getText() == 'SigmaW' and not leftlist[i].getText() in ['RAM[SigmaA]']:
                print('Line ' + str(self.linenumber) + ': SigmaW can only be assigned to RAM[SigmaA]')
                self.state = False
            if leftlist[i].getText() == 'CC' and not type(rightlist[i]) == ALUcc:
                print('Line ' + str(self.linenumber) + ': assignment to CC can only come from ALUcc')
                self.state = False
            if type(rightlist[i]) == ALUcc and not leftlist[i].getText() == 'CC':
                print('Line ' + str(self.linenumber) + ': assignment from ALUcc can only go to CC')
                self.state = False

        #Scan the leftlist to see if there are targets that require the use of the ALU. 
        #Raise an error if this occurs twice.
        aluactivity = False
        for i in range(len(leftlist)):
           if (leftlist[i].getText() in ['IP','RA','RB','IR','_']):
               if not ((leftlist[i].getText() == 'IR') and equal(rightlist[i],Register('IPB'))):
                   if not (leftlist[i].getText() == 'IR') & equal(rightlist[i],RAM(Register('SigmaA'))):
                       if aluactivity:
                           print("ALU used twice in transfer: " + ctx.getText())
                           self.state = False
                       aluactivity = True
               
        #Finally, check when ALUcc is used whether the ALU was actually active
        for i in range(len(leftlist)):
           if type(rightlist[i]) == ALUcc:               
               if not aluactivity:
                   print("ALU.cc undefined in transfer: " + ctx.getText())
                   self.state = False                   
        return

    def visitCommentLine(self, ctx:RTLParser.RegisterTransferContext):
        #comments are skipped
        return
    
    def visitRegisterListLHS(self, ctx:RTLParser.RegisterListLHSContext):
        #create a list of register names
        #fill in the value register for RAM[register]
        if ctx.getChildCount()==1:
            return [self.visit(ctx.registerExpressionLHS()).simplify()]
        else:
            return [self.visit(ctx.registerExpressionLHS()).simplify()] + self.visit(ctx.registerListLHS())

    def visitRegisterListRHS(self, ctx:RTLParser.RegisterListRHSContext):
        #create a list of filled-in register names
        if ctx.getChildCount()==1:
            return [self.visit(ctx.registerExpressionRHS()).simplify()]
        else:
            return [self.visit(ctx.registerExpressionRHS()).simplify()] + self.visit(ctx.registerListRHS())
    
    def visitLeftRegisterName(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #check if the RegisterName that was found can be assigned to in this architecture
        if not ctx.RegisterName().getText() in ['IP','IR','RA','RB','CC','SigmaA','SigmaW','SigmaR','_']:
            print('Line ' + str(self.linenumber) + ': register name ' + ctx.RegisterName().getText() + ' cannot be assigned to in this architecture')
            self.state = False
        #return a RegisterExpression containing exactly the register with the indicated name        
        return Register(ctx.RegisterName().getText())

    def visitLeftRAMAssign(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #check if RegisterName contains a valid adressing register
        if not ctx.RegisterName().getText() in ['SigmaA']:
            print('Line ' + str(self.linenumber) + ': register name ' + ctx.RegisterName().getText() + ' is not a valid adressing register in this architecture')
            self.state = False        
        #fill in the value of the address                
        return RAM(Register(ctx.RegisterName().getText()))

    def visitRightRAMAssign(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #check if RegisterName contains a valid adressing register
        if not ctx.RegisterName().getText() in ['SigmaA']:
            print('Line ' + str(self.linenumber) + ': register name ' + ctx.RegisterName().getText() + ' is not a valid adressing register in this architecture')
            self.state = False        
        #fill in the value of the address                
        return RAM(Register(ctx.RegisterName().getText()))

    def visitRightCCAssign(self, ctx:RTLParser.RegisterExpressionRHSContext):
        return ALUcc(Register('_'))

    def visitRightRegisterName(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #check if RegisterName can be used as an argument
        if not ctx.RegisterName().getText() in ['IP','RA','RB','SigmaW','SigmaR','IR.compl','IR.val']:
            print('Line ' + str(self.linenumber) + ': register name ' + ctx.RegisterName().getText() + ' cannot be used as an argument in this architecture')
            self.state = False

        #check if implicit bus-usage is correct
        if ctx.RegisterName().getText() == 'IR.compl':
            if 'Bbus' in self.bus:
                print('Line ' + str(self.linenumber) + ': IR.compl requires use of Bbus, which is already in use')
                self.state = False
            else:
                self.bus = self.bus + ['Bbus']
        elif ctx.RegisterName().getText() == 'IR.val':
            if 'Bbus' in self.bus:
                print('Line ' + str(self.linenumber) + ': IR.val requires use of Bbus, which is already in use')
                self.state = False
            else:
                self.bus = self.bus + ['Bbus']
        elif ctx.RegisterName().getText() == 'SigmaR':
            if 'Bbus' in self.bus:
                print('Line ' + str(self.linenumber) + ': SigmaR requires use of Bbus, which is already in use')
                self.state = False
            else:
                self.bus = self.bus + ['Bbus']        
            
        return Register(ctx.RegisterName().getText())

    def visitRightOperationPass(self, ctx:RTLParser.RegisterExpressionRHSContext):
        return self.visit(ctx.rb())

    def visitRightOperationAdd(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        if not (('Abus' in self.bus) and ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': Abus and Bbus should both be assigned when using ALU add')
            self.state = False
        return AddRegister(x,y)
    
    def visitRightOperationSub(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        if not (('Abus' in self.bus) and ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': Abus and Bbus should both be assigned when using ALU sub')
            self.state = False        
        return SubRegister(x,y)
    
    def visitRightOperationInc(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        if not (('Abus' in self.bus) or ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': either Abus or Bbus should be assigned when using ALU inc')
            self.state = False        
        return IncRegister(x)

    def visitRightSpecialInc(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #note that in the extended architecture, this notation is used to highlight the special addition.
        #In the simple architecture, it is forbidden.
        print('Line ' + str(self.linenumber) + ': bus usage must be made explicit when using ALU')
        self.state = False
        return IncRegister(Register(ctx.RegisterName().getText()))     
    
    def visitRightOperationDec(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        if not (('Abus' in self.bus) or ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': either Abus or Bbus should be assigned when using ALU dec')
            self.state = False          
        return DecRegister(x)
    
    def visitRightOperationAnd(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        if not (('Abus' in self.bus) and ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': Abus and Bbus should both be assigned when using ALU and')
            self.state = False        
        return AndRegister(x,y)
    
    def visitRightOperationOr(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        if not (('Abus' in self.bus) and ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': Abus and Bbus should both be assigned when using ALU or')
            self.state = False        
        return OrRegister(x,y)
    
    def visitRightOperationComp(self, ctx:RTLParser.RegisterExpressionRHSContext):
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        if not (('Abus' in self.bus) and ('Bbus' in self.bus)):
            print('Line ' + str(self.linenumber) + ': Abus and Bbus should both be assigned when using ALU comp')
            self.state = False        
        return CompRegister(x,y)
    
    def visitRb(self, ctx:RTLParser.RbContext):
        if not ctx.RegisterName().getText() in ['IP','RA','RB','SigmaW','SigmaR','IR.compl','IR.val']:
            print('Line ' + str(self.linenumber) + ': register name ' + ctx.RegisterName().getText() + ' is invalid here')
            self.state = False
        elif ctx.RegisterName().getText() == 'SigmaW':
            print('Line ' + str(self.linenumber) + ': SigmaW has an implicit bus')
            self.state = False
        elif ctx.RegisterName().getText() == 'SigmaR' and not ctx.BusName().getText == 'Bbus':
            print('Line ' + str(self.linenumber) + ': SigmaR cannot be used with bus ' + ctx.BusName().getText())
            self.state = False
        elif ctx.RegisterName().getText() == 'IR.compl' and not ctx.BusName().getText == 'Bbus':
            print('Line ' + str(self.linenumber) + ': IR.compl cannot be used with bus ' + ctx.BusName().getText())
            self.state = False
        elif ctx.RegisterName().getText() == 'IR.val' and not ctx.BusName().getText == 'Bbus':
            print('Line ' + str(self.linenumber) + ': IR.val cannot be used with bus ' + ctx.BusName().getText())
            self.state = False

        #check valid busname
        if not ctx.BusName().getText() in ['Abus','Bbus']:
            print('Line ' + str(self.linenumber) + ': use of bus ' + ctx.BusName().getText() + ' is invalid for this architecture')
            self.state = False
        
        #check if busname is already in use
        if ctx.BusName().getText() in self.bus:
            print('Line ' + str(self.linenumber) + ': bus ' + ctx.BusName().getText() + ' is already in use')
            self.state = False
        else:
            self.bus = self.bus + [ctx.BusName().getText()]
        
        #check if busname is trivial, if so do not change registername, if not append busname to registername        
        if not ctx.RegisterName().getText() in ['IP','RA','RB']:
            return Register(ctx.RegisterName().getText())
        else:            
            return Register(ctx.RegisterName().getText()+':'+ctx.BusName().getText())
 

