# Generated from RTL.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RTLParser import RTLParser
else:
    from RTLParser import RTLParser

# This class defines a complete listener for a parse tree produced by RTLParser.
class RTLListener(ParseTreeListener):

    # Enter a parse tree produced by RTLParser#program.
    def enterProgram(self, ctx:RTLParser.ProgramContext):
        pass

    # Exit a parse tree produced by RTLParser#program.
    def exitProgram(self, ctx:RTLParser.ProgramContext):
        pass


    # Enter a parse tree produced by RTLParser#registerTransfer.
    def enterRegisterTransfer(self, ctx:RTLParser.RegisterTransferContext):
        pass

    # Exit a parse tree produced by RTLParser#registerTransfer.
    def exitRegisterTransfer(self, ctx:RTLParser.RegisterTransferContext):
        pass


    # Enter a parse tree produced by RTLParser#commentLine.
    def enterCommentLine(self, ctx:RTLParser.CommentLineContext):
        pass

    # Exit a parse tree produced by RTLParser#commentLine.
    def exitCommentLine(self, ctx:RTLParser.CommentLineContext):
        pass


    # Enter a parse tree produced by RTLParser#registerListLHS.
    def enterRegisterListLHS(self, ctx:RTLParser.RegisterListLHSContext):
        pass

    # Exit a parse tree produced by RTLParser#registerListLHS.
    def exitRegisterListLHS(self, ctx:RTLParser.RegisterListLHSContext):
        pass


    # Enter a parse tree produced by RTLParser#registerListRHS.
    def enterRegisterListRHS(self, ctx:RTLParser.RegisterListRHSContext):
        pass

    # Exit a parse tree produced by RTLParser#registerListRHS.
    def exitRegisterListRHS(self, ctx:RTLParser.RegisterListRHSContext):
        pass


    # Enter a parse tree produced by RTLParser#leftRegisterName.
    def enterLeftRegisterName(self, ctx:RTLParser.LeftRegisterNameContext):
        pass

    # Exit a parse tree produced by RTLParser#leftRegisterName.
    def exitLeftRegisterName(self, ctx:RTLParser.LeftRegisterNameContext):
        pass


    # Enter a parse tree produced by RTLParser#leftRAMAssign.
    def enterLeftRAMAssign(self, ctx:RTLParser.LeftRAMAssignContext):
        pass

    # Exit a parse tree produced by RTLParser#leftRAMAssign.
    def exitLeftRAMAssign(self, ctx:RTLParser.LeftRAMAssignContext):
        pass


    # Enter a parse tree produced by RTLParser#rightRAMAssign.
    def enterRightRAMAssign(self, ctx:RTLParser.RightRAMAssignContext):
        pass

    # Exit a parse tree produced by RTLParser#rightRAMAssign.
    def exitRightRAMAssign(self, ctx:RTLParser.RightRAMAssignContext):
        pass


    # Enter a parse tree produced by RTLParser#rightCCAssign.
    def enterRightCCAssign(self, ctx:RTLParser.RightCCAssignContext):
        pass

    # Exit a parse tree produced by RTLParser#rightCCAssign.
    def exitRightCCAssign(self, ctx:RTLParser.RightCCAssignContext):
        pass


    # Enter a parse tree produced by RTLParser#rightRegisterName.
    def enterRightRegisterName(self, ctx:RTLParser.RightRegisterNameContext):
        pass

    # Exit a parse tree produced by RTLParser#rightRegisterName.
    def exitRightRegisterName(self, ctx:RTLParser.RightRegisterNameContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationPass.
    def enterRightOperationPass(self, ctx:RTLParser.RightOperationPassContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationPass.
    def exitRightOperationPass(self, ctx:RTLParser.RightOperationPassContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationAdd.
    def enterRightOperationAdd(self, ctx:RTLParser.RightOperationAddContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationAdd.
    def exitRightOperationAdd(self, ctx:RTLParser.RightOperationAddContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationSub.
    def enterRightOperationSub(self, ctx:RTLParser.RightOperationSubContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationSub.
    def exitRightOperationSub(self, ctx:RTLParser.RightOperationSubContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationInc.
    def enterRightOperationInc(self, ctx:RTLParser.RightOperationIncContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationInc.
    def exitRightOperationInc(self, ctx:RTLParser.RightOperationIncContext):
        pass


    # Enter a parse tree produced by RTLParser#rightSpecialInc.
    def enterRightSpecialInc(self, ctx:RTLParser.RightSpecialIncContext):
        pass

    # Exit a parse tree produced by RTLParser#rightSpecialInc.
    def exitRightSpecialInc(self, ctx:RTLParser.RightSpecialIncContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationDec.
    def enterRightOperationDec(self, ctx:RTLParser.RightOperationDecContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationDec.
    def exitRightOperationDec(self, ctx:RTLParser.RightOperationDecContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationAnd.
    def enterRightOperationAnd(self, ctx:RTLParser.RightOperationAndContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationAnd.
    def exitRightOperationAnd(self, ctx:RTLParser.RightOperationAndContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationOr.
    def enterRightOperationOr(self, ctx:RTLParser.RightOperationOrContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationOr.
    def exitRightOperationOr(self, ctx:RTLParser.RightOperationOrContext):
        pass


    # Enter a parse tree produced by RTLParser#rightOperationComp.
    def enterRightOperationComp(self, ctx:RTLParser.RightOperationCompContext):
        pass

    # Exit a parse tree produced by RTLParser#rightOperationComp.
    def exitRightOperationComp(self, ctx:RTLParser.RightOperationCompContext):
        pass


    # Enter a parse tree produced by RTLParser#rb.
    def enterRb(self, ctx:RTLParser.RbContext):
        pass

    # Exit a parse tree produced by RTLParser#rb.
    def exitRb(self, ctx:RTLParser.RbContext):
        pass


