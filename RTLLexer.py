# Generated from RTL.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\24")
        buf.write("\u00a0\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3")
        buf.write("\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\16\3\16")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\5\17|\n\17\3\20\3\20\3\20\3\20\3\20\3\20\3")
        buf.write("\20\3\20\3\20\3\20\3\20\3\20\5\20\u008a\n\20\3\21\3\21")
        buf.write("\7\21\u008e\n\21\f\21\16\21\u0091\13\21\3\21\3\21\3\22")
        buf.write("\5\22\u0096\n\22\3\22\3\22\3\23\6\23\u009b\n\23\r\23\16")
        buf.write("\23\u009c\3\23\3\23\2\2\24\3\3\5\4\7\5\t\6\13\7\r\b\17")
        buf.write("\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23")
        buf.write("%\24\3\2\4\3\2\f\f\4\2\13\13\"\"\2\u00af\2\3\3\2\2\2\2")
        buf.write("\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3")
        buf.write("\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2")
        buf.write("\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2")
        buf.write("\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\3\'\3")
        buf.write("\2\2\2\5*\3\2\2\2\7,\3\2\2\2\t\61\3\2\2\2\13\63\3\2\2")
        buf.write("\2\r:\3\2\2\2\17<\3\2\2\2\21>\3\2\2\2\23@\3\2\2\2\25C")
        buf.write("\3\2\2\2\27F\3\2\2\2\31I\3\2\2\2\33K\3\2\2\2\35{\3\2\2")
        buf.write("\2\37\u0089\3\2\2\2!\u008b\3\2\2\2#\u0095\3\2\2\2%\u009a")
        buf.write("\3\2\2\2\'(\7>\2\2()\7/\2\2)\4\3\2\2\2*+\7.\2\2+\6\3\2")
        buf.write("\2\2,-\7T\2\2-.\7C\2\2./\7O\2\2/\60\7]\2\2\60\b\3\2\2")
        buf.write("\2\61\62\7_\2\2\62\n\3\2\2\2\63\64\7C\2\2\64\65\7N\2\2")
        buf.write("\65\66\7W\2\2\66\67\7\60\2\2\678\7e\2\289\7e\2\29\f\3")
        buf.write("\2\2\2:;\7-\2\2;\16\3\2\2\2<=\7/\2\2=\20\3\2\2\2>?\7\63")
        buf.write("\2\2?\22\3\2\2\2@A\7(\2\2AB\7(\2\2B\24\3\2\2\2CD\7~\2")
        buf.write("\2DE\7~\2\2E\26\3\2\2\2FG\7#\2\2GH\7?\2\2H\30\3\2\2\2")
        buf.write("IJ\7*\2\2J\32\3\2\2\2KL\7+\2\2L\34\3\2\2\2MN\7K\2\2N|")
        buf.write("\7R\2\2OP\7K\2\2PQ\7R\2\2Q|\7D\2\2RS\7K\2\2S|\7T\2\2T")
        buf.write("U\7T\2\2U|\7C\2\2VW\7T\2\2W|\7D\2\2X|\7a\2\2YZ\7E\2\2")
        buf.write("Z|\7E\2\2[\\\7U\2\2\\]\7k\2\2]^\7i\2\2^_\7o\2\2_`\7c\2")
        buf.write("\2`|\7C\2\2ab\7U\2\2bc\7k\2\2cd\7i\2\2de\7o\2\2ef\7c\2")
        buf.write("\2f|\7Y\2\2gh\7U\2\2hi\7k\2\2ij\7i\2\2jk\7o\2\2kl\7c\2")
        buf.write("\2l|\7T\2\2mn\7K\2\2no\7T\2\2op\7\60\2\2pq\7e\2\2qr\7")
        buf.write("q\2\2rs\7o\2\2st\7r\2\2t|\7n\2\2uv\7K\2\2vw\7T\2\2wx\7")
        buf.write("\60\2\2xy\7x\2\2yz\7c\2\2z|\7n\2\2{M\3\2\2\2{O\3\2\2\2")
        buf.write("{R\3\2\2\2{T\3\2\2\2{V\3\2\2\2{X\3\2\2\2{Y\3\2\2\2{[\3")
        buf.write("\2\2\2{a\3\2\2\2{g\3\2\2\2{m\3\2\2\2{u\3\2\2\2|\36\3\2")
        buf.write("\2\2}~\7C\2\2~\177\7d\2\2\177\u0080\7w\2\2\u0080\u008a")
        buf.write("\7u\2\2\u0081\u0082\7D\2\2\u0082\u0083\7d\2\2\u0083\u0084")
        buf.write("\7w\2\2\u0084\u008a\7u\2\2\u0085\u0086\7K\2\2\u0086\u0087")
        buf.write("\7d\2\2\u0087\u0088\7w\2\2\u0088\u008a\7u\2\2\u0089}\3")
        buf.write("\2\2\2\u0089\u0081\3\2\2\2\u0089\u0085\3\2\2\2\u008a ")
        buf.write("\3\2\2\2\u008b\u008f\7}\2\2\u008c\u008e\n\2\2\2\u008d")
        buf.write("\u008c\3\2\2\2\u008e\u0091\3\2\2\2\u008f\u008d\3\2\2\2")
        buf.write("\u008f\u0090\3\2\2\2\u0090\u0092\3\2\2\2\u0091\u008f\3")
        buf.write("\2\2\2\u0092\u0093\7\177\2\2\u0093\"\3\2\2\2\u0094\u0096")
        buf.write("\7\17\2\2\u0095\u0094\3\2\2\2\u0095\u0096\3\2\2\2\u0096")
        buf.write("\u0097\3\2\2\2\u0097\u0098\7\f\2\2\u0098$\3\2\2\2\u0099")
        buf.write("\u009b\t\3\2\2\u009a\u0099\3\2\2\2\u009b\u009c\3\2\2\2")
        buf.write("\u009c\u009a\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\3")
        buf.write("\2\2\2\u009e\u009f\b\23\2\2\u009f&\3\2\2\2\b\2{\u0089")
        buf.write("\u008f\u0095\u009c\3\b\2\2")
        return buf.getvalue()


class RTLLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    RegisterName = 14
    BusName = 15
    Comment = 16
    NEWLINE = 17
    WS = 18

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'<-'", "','", "'RAM['", "']'", "'ALU.cc'", "'+'", "'-'", "'1'", 
            "'&&'", "'||'", "'!='", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "RegisterName", "BusName", "Comment", "NEWLINE", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "RegisterName", 
                  "BusName", "Comment", "NEWLINE", "WS" ]

    grammarFileName = "RTL.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


