import pickle
from RegisterExpression import *

precondition = {}
precondition["IP"] = Register("ip")
precondition["IPB"] = Register("ipb")
precondition["IR.compl"] = Register("ir.compl")
precondition["IR.val"] = Register("ir.val")
precondition["RA"] = Register("ra")
precondition["RB"] = Register("rb")
precondition["CC"] = Register("cc")
precondition["SigmaA"] = Register("sigmaa")
precondition["SigmaW"] = Register("sigmaw")
precondition["SigmaR"] = Register("sigmar")

afile = open('config.pkl','wb')
pickle.dump(precondition, afile)
afile.close()
