# Generated from RTL.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .RTLParser import RTLParser
else:
    from RTLParser import RTLParser

# This class defines a complete generic visitor for a parse tree produced by RTLParser.

class RTLVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by RTLParser#program.
    def visitProgram(self, ctx:RTLParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#registerTransfer.
    def visitRegisterTransfer(self, ctx:RTLParser.RegisterTransferContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#commentLine.
    def visitCommentLine(self, ctx:RTLParser.CommentLineContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#registerListLHS.
    def visitRegisterListLHS(self, ctx:RTLParser.RegisterListLHSContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#registerListRHS.
    def visitRegisterListRHS(self, ctx:RTLParser.RegisterListRHSContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#leftRegisterName.
    def visitLeftRegisterName(self, ctx:RTLParser.LeftRegisterNameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#leftRAMAssign.
    def visitLeftRAMAssign(self, ctx:RTLParser.LeftRAMAssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightRAMAssign.
    def visitRightRAMAssign(self, ctx:RTLParser.RightRAMAssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightCCAssign.
    def visitRightCCAssign(self, ctx:RTLParser.RightCCAssignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightRegisterName.
    def visitRightRegisterName(self, ctx:RTLParser.RightRegisterNameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationPass.
    def visitRightOperationPass(self, ctx:RTLParser.RightOperationPassContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationAdd.
    def visitRightOperationAdd(self, ctx:RTLParser.RightOperationAddContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationSub.
    def visitRightOperationSub(self, ctx:RTLParser.RightOperationSubContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationInc.
    def visitRightOperationInc(self, ctx:RTLParser.RightOperationIncContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightSpecialInc.
    def visitRightSpecialInc(self, ctx:RTLParser.RightSpecialIncContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationDec.
    def visitRightOperationDec(self, ctx:RTLParser.RightOperationDecContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationAnd.
    def visitRightOperationAnd(self, ctx:RTLParser.RightOperationAndContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationOr.
    def visitRightOperationOr(self, ctx:RTLParser.RightOperationOrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rightOperationComp.
    def visitRightOperationComp(self, ctx:RTLParser.RightOperationCompContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by RTLParser#rb.
    def visitRb(self, ctx:RTLParser.RbContext):
        return self.visitChildren(ctx)



del RTLParser