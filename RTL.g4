// Define a grammar called RTL (Register Transfer Language)
//
// To build a parser for it using Antlr type (in the working directory):
// java org.antlr.v4.Tool -Dlanguage=Python3 -visitor RTL.g4
//
grammar RTL;
program                 : line*;
line                    : registerListLHS '<-' registerListRHS NEWLINE?   # registerTransfer
                        | Comment NEWLINE?                                # commentLine
                        ;

registerListLHS         : registerExpressionLHS (',' registerListLHS)?;
registerListRHS         : registerExpressionRHS (',' registerListRHS)?;

registerExpressionLHS   : RegisterName                                    # leftRegisterName
                        | 'RAM[' RegisterName ']'                         # leftRAMAssign
                        ;

registerExpressionRHS   : 'RAM[' RegisterName ']'                         # rightRAMAssign
                        | 'ALU.cc'                                        # rightCCAssign
                        | RegisterName                                    # rightRegisterName
                        | rb                                              # rightOperationPass
                        | rb '+' rb                                       # rightOperationAdd
                        | rb '-' rb                                       # rightOperationSub
                        | rb '+' '1'                                      # rightOperationInc
                        | RegisterName '+' '1'                            # rightSpecialInc
                        | rb '-' '1'                                      # rightOperationDec
                        | rb '&&' rb                                      # rightOperationAnd
                        | rb '||' rb                                      # rightOperationOr
                        | rb '!=' rb                                      # rightOperationComp
                        ;

rb                      : RegisterName '(' BusName ')' ;

RegisterName            : 'IP' | 'IPB' | 'IR' | 'RA' | 'RB' | '_'
                        | 'CC' | 'SigmaA' | 'SigmaW' | 'SigmaR' 
                        | 'IR.compl' | 'IR.val' 
                        ;
                                                
BusName                 : 'Abus' | 'Bbus' | 'Ibus' ;                      
                        
Comment                 : '{' (~'\n')* '}';

NEWLINE                 : '\r'? '\n';
WS                      : [ \t]+ -> skip ;
