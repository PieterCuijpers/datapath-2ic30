# This visitor prints a RTL program

from RTLParser import RTLParser
from RTLVisitor import RTLVisitor
from RegisterExpression import *

class PrintVisitor(RTLVisitor):
 
    def __init__(self):
        self.linenumber = 0
        pass
    
    def visitProgram(self, ctx:RTLParser.ProgramContext):
        self.visitChildren(ctx)
        return

    def visitRegisterTransfer(self, ctx:RTLParser.LineContext):
        self.linenumber = self.linenumber + 1
        print(str(self.linenumber) + ': ' + ctx.getText())
        return

    def visitCommentLine(self, ctx:RTLParser.RegisterTransferContext):
        #comments are skipped
        return
