# This visitor implements the execution of a register transfer language program
# For a given symbolic precondition, it calculates the symbolic postcondition
#

from RTLParser import RTLParser
from RTLVisitor import RTLVisitor
from RegisterExpression import *

class ComputeVisitor(RTLVisitor):
 
    def __init__(self,precondition:{}):
        if len(precondition)==0:
            self.state = {}
            self.state["IP"] = Register("undefined")
            self.state["IPB"] = Register("undefined")
            self.state["IR"] = Register("undefined")
            self.state["IR.compl"] = Register("undefined")
            self.state["IR.val"] = Register("undefined")
            self.state["RA"] = Register("undefined")
            self.state["RB"] = Register("undefined")
            self.state["CC"] = Register("undefined")
            self.state["SigmaA"] = Register("undefined")
            self.state["SigmaW"] = Register("undefined")
            self.state["SigmaR"] = Register("undefined")            
        else:
            self.state = precondition        

    def visitProgram(self, ctx:RTLParser.ProgramContext):        
        self.visitChildren(ctx)
        return self.state

    def visitRegisterTransfer(self, ctx:RTLParser.LineContext):
        #A transfer RA,RB,RC <- Exp1, Exp2, Exp3 consists of two lists of the same length.
        #The leftlist contains strings with the names of target registers, while the rightlist contains
        #more complex register expressions that need to be placed in the target registers
        #Note that for the leftlist we use strings instead of expressions to make it easier to identify them

        self.detectSpecialInc = False;                 #Detect the use of IP+1, an addition that bypasses the ALU
        leftlist = self.visit(ctx.registerListLHS())   #Returns a list of RegisterExpressions with basic Registers
        rightlist = self.visit(ctx.registerListRHS())  #Returns a list of more complex RegisterExpressions
        
        #In case the ALU.cc flag needs to be set, we need to remember the use of the ALU elsewhere
        #in the register transfer. Therefore, we next scan the leftlist to see if there are targets
        #that require the use of the ALU, and subsequently remember that use.
        #Above, we detected the use of IP+1 in the extended architecture, which needs to be filtered
        #out because the ALU is bypassed in that case. In the basic architecture, the use of IP+1
        #should be written IP(Abus)+1, which explicitly aims at the ALU.
        aluactivity = None
        for i in range(len(leftlist)):
           if (leftlist[i] in ['IP','RA','RB','IR','_']):
               if not self.detectSpecialInc:
                   if not ((leftlist[i] == 'IR') and equal(rightlist[i],Register('IPB'))):
                       if not (leftlist[i] == 'IR') & equal(rightlist[i],RAM(Register('SigmaA'))):
                           aluactivity = rightlist[i]
               
        #Finally, we copy the content of the rightlist into the leftlist
        #with a special arrangement for the ALUcc, which is replaced by
        #the alu-activity calculated in the previous step
        #Also, there is a special arrangment for the register '_', which
        #is skipped because it is only used to denote the use of an ALU
        #with the sole purpose of calculating ALU.cc
        for i in range(len(leftlist)):
           if not leftlist[i] == '_':
               self.state[leftlist[i]] = rightlist[i]
           if type(rightlist[i]) == ALUcc:               
               self.state[leftlist[i]] = ALUcc(aluactivity)
        return

    def visitCommentLine(self, ctx:RTLParser.RegisterTransferContext):
        #comments are skipped
        return 
    
    def visitRegisterListLHS(self, ctx:RTLParser.RegisterListLHSContext):
        #create a list of strings with register names occuring on the left side of a transfer
        #(note that when RAM[register] occurs on the left side, the value of the register is filled in)
        if ctx.getChildCount()==1:
            return [self.visit(ctx.registerExpressionLHS()).simplify().getText()]
        else:
            return [self.visit(ctx.registerExpressionLHS()).simplify().getText()] + self.visit(ctx.registerListLHS())

    def visitRegisterListRHS(self, ctx:RTLParser.RegisterListRHSContext):
        #create a list of filled-in register names occuring on the right side of a transfer
        if ctx.getChildCount()==1:
            return [self.visit(ctx.registerExpressionRHS()).simplify()]
        else:
            return [self.visit(ctx.registerExpressionRHS()).simplify()] + self.visit(ctx.registerListRHS())
    
    def visitLeftRegisterName(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #return a RegisterExpression containing exactly the register with the indicated name        
        return Register(ctx.RegisterName().getText())

    def visitLeftRAMAssign(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #fill in the value of the address                
        return RAM(self.state[ctx.RegisterName().getText()])

    def visitRightRAMAssign(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #fill in the value of the address                
        return RAM(self.state[ctx.RegisterName().getText()])

    def visitRightCCAssign(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return a register expression corresponding to ALUcc
        return ALUcc(Register('_'))

    def visitRightRegisterName(self, ctx:RTLParser.RegisterExpressionLHSContext):
        #fill in the value of the register with the given name
        return self.state[ctx.RegisterName().getText()]

    def visitRightOperationPass(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #fill in the value of the register with the given name
        return self.visit(ctx.rb())

    def visitRightOperationAdd(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the addition of the two (filled in) sub-expressions
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        return AddRegister(x,y)
    
    def visitRightOperationSub(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the subtraction of the two (filled in) sub-expressions
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        return SubRegister(x,y)
    
    def visitRightOperationInc(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the increment of the sub-expression
        x = self.visit(ctx.getChild(0))        
        return IncRegister(x)

    def visitRightSpecialInc(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the increment of the sub-expression,
        #and remember the use of the IP+1 increment occurred in this transfer
        self.detectSpecialInc = True
        return IncRegister(self.state[ctx.RegisterName().getText()])     
    
    def visitRightOperationDec(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the decrement of the sub-expression
        x = self.visit(ctx.getChild(0))        
        return DecRegister(x)
    
    def visitRightOperationAnd(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the conjunction of the two (filled in) sub-expressions
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        return AndRegister(x,y)
    
    def visitRightOperationOr(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the disjunction of the two (filled in) sub-expressions
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        return OrRegister(x,y)
    
    def visitRightOperationComp(self, ctx:RTLParser.RegisterExpressionRHSContext):
        #return the comparison of the two (filled in) sub-expressions
        x = self.visit(ctx.getChild(0))
        y = self.visit(ctx.getChild(2))
        return CompRegister(x,y)
    
    def visitRb(self, ctx:RTLParser.RbContext):
        #fill in the value of the register, pointed out in the RegisterName (BusName) pair.
        return self.state[ctx.RegisterName().getText()]
