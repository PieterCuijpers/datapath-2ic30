# Generated from RTL.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24")
        buf.write("_\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\3\2\7\2\22\n\2\f\2\16\2\25\13\2\3\3\3\3\3\3\3\3\5")
        buf.write("\3\33\n\3\3\3\3\3\5\3\37\n\3\5\3!\n\3\3\4\3\4\3\4\5\4")
        buf.write("&\n\4\3\5\3\5\3\5\5\5+\n\5\3\6\3\6\3\6\3\6\5\6\61\n\6")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7X\n\7\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\2\2\t\2\4\6\b\n\f\16\2\2\2i\2\23")
        buf.write("\3\2\2\2\4 \3\2\2\2\6\"\3\2\2\2\b\'\3\2\2\2\n\60\3\2\2")
        buf.write("\2\fW\3\2\2\2\16Y\3\2\2\2\20\22\5\4\3\2\21\20\3\2\2\2")
        buf.write("\22\25\3\2\2\2\23\21\3\2\2\2\23\24\3\2\2\2\24\3\3\2\2")
        buf.write("\2\25\23\3\2\2\2\26\27\5\6\4\2\27\30\7\3\2\2\30\32\5\b")
        buf.write("\5\2\31\33\7\23\2\2\32\31\3\2\2\2\32\33\3\2\2\2\33!\3")
        buf.write("\2\2\2\34\36\7\22\2\2\35\37\7\23\2\2\36\35\3\2\2\2\36")
        buf.write("\37\3\2\2\2\37!\3\2\2\2 \26\3\2\2\2 \34\3\2\2\2!\5\3\2")
        buf.write("\2\2\"%\5\n\6\2#$\7\4\2\2$&\5\6\4\2%#\3\2\2\2%&\3\2\2")
        buf.write("\2&\7\3\2\2\2\'*\5\f\7\2()\7\4\2\2)+\5\b\5\2*(\3\2\2\2")
        buf.write("*+\3\2\2\2+\t\3\2\2\2,\61\7\20\2\2-.\7\5\2\2./\7\20\2")
        buf.write("\2/\61\7\6\2\2\60,\3\2\2\2\60-\3\2\2\2\61\13\3\2\2\2\62")
        buf.write("\63\7\5\2\2\63\64\7\20\2\2\64X\7\6\2\2\65X\7\7\2\2\66")
        buf.write("X\7\20\2\2\67X\5\16\b\289\5\16\b\29:\7\b\2\2:;\5\16\b")
        buf.write("\2;X\3\2\2\2<=\5\16\b\2=>\7\t\2\2>?\5\16\b\2?X\3\2\2\2")
        buf.write("@A\5\16\b\2AB\7\b\2\2BC\7\n\2\2CX\3\2\2\2DE\7\20\2\2E")
        buf.write("F\7\b\2\2FX\7\n\2\2GH\5\16\b\2HI\7\t\2\2IJ\7\n\2\2JX\3")
        buf.write("\2\2\2KL\5\16\b\2LM\7\13\2\2MN\5\16\b\2NX\3\2\2\2OP\5")
        buf.write("\16\b\2PQ\7\f\2\2QR\5\16\b\2RX\3\2\2\2ST\5\16\b\2TU\7")
        buf.write("\r\2\2UV\5\16\b\2VX\3\2\2\2W\62\3\2\2\2W\65\3\2\2\2W\66")
        buf.write("\3\2\2\2W\67\3\2\2\2W8\3\2\2\2W<\3\2\2\2W@\3\2\2\2WD\3")
        buf.write("\2\2\2WG\3\2\2\2WK\3\2\2\2WO\3\2\2\2WS\3\2\2\2X\r\3\2")
        buf.write("\2\2YZ\7\20\2\2Z[\7\16\2\2[\\\7\21\2\2\\]\7\17\2\2]\17")
        buf.write("\3\2\2\2\n\23\32\36 %*\60W")
        return buf.getvalue()


class RTLParser ( Parser ):

    grammarFileName = "RTL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'<-'", "','", "'RAM['", "']'", "'ALU.cc'", 
                     "'+'", "'-'", "'1'", "'&&'", "'||'", "'!='", "'('", 
                     "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "RegisterName", "BusName", 
                      "Comment", "NEWLINE", "WS" ]

    RULE_program = 0
    RULE_line = 1
    RULE_registerListLHS = 2
    RULE_registerListRHS = 3
    RULE_registerExpressionLHS = 4
    RULE_registerExpressionRHS = 5
    RULE_rb = 6

    ruleNames =  [ "program", "line", "registerListLHS", "registerListRHS", 
                   "registerExpressionLHS", "registerExpressionRHS", "rb" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    RegisterName=14
    BusName=15
    Comment=16
    NEWLINE=17
    WS=18

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def line(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.LineContext)
            else:
                return self.getTypedRuleContext(RTLParser.LineContext,i)


        def getRuleIndex(self):
            return RTLParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = RTLParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RTLParser.T__2) | (1 << RTLParser.RegisterName) | (1 << RTLParser.Comment))) != 0):
                self.state = 14
                self.line()
                self.state = 19
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LineContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RTLParser.RULE_line

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class RegisterTransferContext(LineContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.LineContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def registerListLHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterListLHSContext,0)

        def registerListRHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterListRHSContext,0)

        def NEWLINE(self):
            return self.getToken(RTLParser.NEWLINE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegisterTransfer" ):
                listener.enterRegisterTransfer(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegisterTransfer" ):
                listener.exitRegisterTransfer(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRegisterTransfer" ):
                return visitor.visitRegisterTransfer(self)
            else:
                return visitor.visitChildren(self)


    class CommentLineContext(LineContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.LineContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def Comment(self):
            return self.getToken(RTLParser.Comment, 0)
        def NEWLINE(self):
            return self.getToken(RTLParser.NEWLINE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCommentLine" ):
                listener.enterCommentLine(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCommentLine" ):
                listener.exitCommentLine(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCommentLine" ):
                return visitor.visitCommentLine(self)
            else:
                return visitor.visitChildren(self)



    def line(self):

        localctx = RTLParser.LineContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_line)
        self._la = 0 # Token type
        try:
            self.state = 30
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RTLParser.T__2, RTLParser.RegisterName]:
                localctx = RTLParser.RegisterTransferContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 20
                self.registerListLHS()
                self.state = 21
                self.match(RTLParser.T__0)
                self.state = 22
                self.registerListRHS()
                self.state = 24
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==RTLParser.NEWLINE:
                    self.state = 23
                    self.match(RTLParser.NEWLINE)


                pass
            elif token in [RTLParser.Comment]:
                localctx = RTLParser.CommentLineContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 26
                self.match(RTLParser.Comment)
                self.state = 28
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==RTLParser.NEWLINE:
                    self.state = 27
                    self.match(RTLParser.NEWLINE)


                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegisterListLHSContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def registerExpressionLHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterExpressionLHSContext,0)


        def registerListLHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterListLHSContext,0)


        def getRuleIndex(self):
            return RTLParser.RULE_registerListLHS

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegisterListLHS" ):
                listener.enterRegisterListLHS(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegisterListLHS" ):
                listener.exitRegisterListLHS(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRegisterListLHS" ):
                return visitor.visitRegisterListLHS(self)
            else:
                return visitor.visitChildren(self)




    def registerListLHS(self):

        localctx = RTLParser.RegisterListLHSContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_registerListLHS)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 32
            self.registerExpressionLHS()
            self.state = 35
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==RTLParser.T__1:
                self.state = 33
                self.match(RTLParser.T__1)
                self.state = 34
                self.registerListLHS()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegisterListRHSContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def registerExpressionRHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterExpressionRHSContext,0)


        def registerListRHS(self):
            return self.getTypedRuleContext(RTLParser.RegisterListRHSContext,0)


        def getRuleIndex(self):
            return RTLParser.RULE_registerListRHS

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRegisterListRHS" ):
                listener.enterRegisterListRHS(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRegisterListRHS" ):
                listener.exitRegisterListRHS(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRegisterListRHS" ):
                return visitor.visitRegisterListRHS(self)
            else:
                return visitor.visitChildren(self)




    def registerListRHS(self):

        localctx = RTLParser.RegisterListRHSContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_registerListRHS)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 37
            self.registerExpressionRHS()
            self.state = 40
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==RTLParser.T__1:
                self.state = 38
                self.match(RTLParser.T__1)
                self.state = 39
                self.registerListRHS()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegisterExpressionLHSContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RTLParser.RULE_registerExpressionLHS

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class LeftRegisterNameContext(RegisterExpressionLHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionLHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLeftRegisterName" ):
                listener.enterLeftRegisterName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLeftRegisterName" ):
                listener.exitLeftRegisterName(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLeftRegisterName" ):
                return visitor.visitLeftRegisterName(self)
            else:
                return visitor.visitChildren(self)


    class LeftRAMAssignContext(RegisterExpressionLHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionLHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLeftRAMAssign" ):
                listener.enterLeftRAMAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLeftRAMAssign" ):
                listener.exitLeftRAMAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLeftRAMAssign" ):
                return visitor.visitLeftRAMAssign(self)
            else:
                return visitor.visitChildren(self)



    def registerExpressionLHS(self):

        localctx = RTLParser.RegisterExpressionLHSContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_registerExpressionLHS)
        try:
            self.state = 46
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RTLParser.RegisterName]:
                localctx = RTLParser.LeftRegisterNameContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 42
                self.match(RTLParser.RegisterName)
                pass
            elif token in [RTLParser.T__2]:
                localctx = RTLParser.LeftRAMAssignContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.match(RTLParser.T__2)
                self.state = 44
                self.match(RTLParser.RegisterName)
                self.state = 45
                self.match(RTLParser.T__3)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegisterExpressionRHSContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RTLParser.RULE_registerExpressionRHS

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class RightOperationAndContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.RbContext)
            else:
                return self.getTypedRuleContext(RTLParser.RbContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationAnd" ):
                listener.enterRightOperationAnd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationAnd" ):
                listener.exitRightOperationAnd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationAnd" ):
                return visitor.visitRightOperationAnd(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationPassContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self):
            return self.getTypedRuleContext(RTLParser.RbContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationPass" ):
                listener.enterRightOperationPass(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationPass" ):
                listener.exitRightOperationPass(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationPass" ):
                return visitor.visitRightOperationPass(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationCompContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.RbContext)
            else:
                return self.getTypedRuleContext(RTLParser.RbContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationComp" ):
                listener.enterRightOperationComp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationComp" ):
                listener.exitRightOperationComp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationComp" ):
                return visitor.visitRightOperationComp(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationOrContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.RbContext)
            else:
                return self.getTypedRuleContext(RTLParser.RbContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationOr" ):
                listener.enterRightOperationOr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationOr" ):
                listener.exitRightOperationOr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationOr" ):
                return visitor.visitRightOperationOr(self)
            else:
                return visitor.visitChildren(self)


    class RightSpecialIncContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightSpecialInc" ):
                listener.enterRightSpecialInc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightSpecialInc" ):
                listener.exitRightSpecialInc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightSpecialInc" ):
                return visitor.visitRightSpecialInc(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationSubContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.RbContext)
            else:
                return self.getTypedRuleContext(RTLParser.RbContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationSub" ):
                listener.enterRightOperationSub(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationSub" ):
                listener.exitRightOperationSub(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationSub" ):
                return visitor.visitRightOperationSub(self)
            else:
                return visitor.visitChildren(self)


    class RightRAMAssignContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightRAMAssign" ):
                listener.enterRightRAMAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightRAMAssign" ):
                listener.exitRightRAMAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightRAMAssign" ):
                return visitor.visitRightRAMAssign(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationDecContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self):
            return self.getTypedRuleContext(RTLParser.RbContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationDec" ):
                listener.enterRightOperationDec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationDec" ):
                listener.exitRightOperationDec(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationDec" ):
                return visitor.visitRightOperationDec(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationAddContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RTLParser.RbContext)
            else:
                return self.getTypedRuleContext(RTLParser.RbContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationAdd" ):
                listener.enterRightOperationAdd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationAdd" ):
                listener.exitRightOperationAdd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationAdd" ):
                return visitor.visitRightOperationAdd(self)
            else:
                return visitor.visitChildren(self)


    class RightCCAssignContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightCCAssign" ):
                listener.enterRightCCAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightCCAssign" ):
                listener.exitRightCCAssign(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightCCAssign" ):
                return visitor.visitRightCCAssign(self)
            else:
                return visitor.visitChildren(self)


    class RightRegisterNameContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightRegisterName" ):
                listener.enterRightRegisterName(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightRegisterName" ):
                listener.exitRightRegisterName(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightRegisterName" ):
                return visitor.visitRightRegisterName(self)
            else:
                return visitor.visitChildren(self)


    class RightOperationIncContext(RegisterExpressionRHSContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RTLParser.RegisterExpressionRHSContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def rb(self):
            return self.getTypedRuleContext(RTLParser.RbContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightOperationInc" ):
                listener.enterRightOperationInc(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightOperationInc" ):
                listener.exitRightOperationInc(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightOperationInc" ):
                return visitor.visitRightOperationInc(self)
            else:
                return visitor.visitChildren(self)



    def registerExpressionRHS(self):

        localctx = RTLParser.RegisterExpressionRHSContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_registerExpressionRHS)
        try:
            self.state = 85
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                localctx = RTLParser.RightRAMAssignContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 48
                self.match(RTLParser.T__2)
                self.state = 49
                self.match(RTLParser.RegisterName)
                self.state = 50
                self.match(RTLParser.T__3)
                pass

            elif la_ == 2:
                localctx = RTLParser.RightCCAssignContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 51
                self.match(RTLParser.T__4)
                pass

            elif la_ == 3:
                localctx = RTLParser.RightRegisterNameContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 52
                self.match(RTLParser.RegisterName)
                pass

            elif la_ == 4:
                localctx = RTLParser.RightOperationPassContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 53
                self.rb()
                pass

            elif la_ == 5:
                localctx = RTLParser.RightOperationAddContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 54
                self.rb()
                self.state = 55
                self.match(RTLParser.T__5)
                self.state = 56
                self.rb()
                pass

            elif la_ == 6:
                localctx = RTLParser.RightOperationSubContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 58
                self.rb()
                self.state = 59
                self.match(RTLParser.T__6)
                self.state = 60
                self.rb()
                pass

            elif la_ == 7:
                localctx = RTLParser.RightOperationIncContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 62
                self.rb()
                self.state = 63
                self.match(RTLParser.T__5)
                self.state = 64
                self.match(RTLParser.T__7)
                pass

            elif la_ == 8:
                localctx = RTLParser.RightSpecialIncContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 66
                self.match(RTLParser.RegisterName)
                self.state = 67
                self.match(RTLParser.T__5)
                self.state = 68
                self.match(RTLParser.T__7)
                pass

            elif la_ == 9:
                localctx = RTLParser.RightOperationDecContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 69
                self.rb()
                self.state = 70
                self.match(RTLParser.T__6)
                self.state = 71
                self.match(RTLParser.T__7)
                pass

            elif la_ == 10:
                localctx = RTLParser.RightOperationAndContext(self, localctx)
                self.enterOuterAlt(localctx, 10)
                self.state = 73
                self.rb()
                self.state = 74
                self.match(RTLParser.T__8)
                self.state = 75
                self.rb()
                pass

            elif la_ == 11:
                localctx = RTLParser.RightOperationOrContext(self, localctx)
                self.enterOuterAlt(localctx, 11)
                self.state = 77
                self.rb()
                self.state = 78
                self.match(RTLParser.T__9)
                self.state = 79
                self.rb()
                pass

            elif la_ == 12:
                localctx = RTLParser.RightOperationCompContext(self, localctx)
                self.enterOuterAlt(localctx, 12)
                self.state = 81
                self.rb()
                self.state = 82
                self.match(RTLParser.T__10)
                self.state = 83
                self.rb()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RbContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RegisterName(self):
            return self.getToken(RTLParser.RegisterName, 0)

        def BusName(self):
            return self.getToken(RTLParser.BusName, 0)

        def getRuleIndex(self):
            return RTLParser.RULE_rb

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRb" ):
                listener.enterRb(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRb" ):
                listener.exitRb(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRb" ):
                return visitor.visitRb(self)
            else:
                return visitor.visitChildren(self)




    def rb(self):

        localctx = RTLParser.RbContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_rb)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 87
            self.match(RTLParser.RegisterName)
            self.state = 88
            self.match(RTLParser.T__11)
            self.state = 89
            self.match(RTLParser.BusName)
            self.state = 90
            self.match(RTLParser.T__12)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





