# This program simulates a spec in Register Transfer Language (RTL)
# and checks with it complies with the basic and extended
# architectures as introduced in the course 2IC30 Computer Systems
#
# Author: Pieter Cuijpers
# P.J.L.Cuijpers@tue.nl
#
# The grammar of RTL is given in RTL.g4 and used by ANTLR to
# generate the necessary classes
#
# Call this program by typing: Python CheckRTL.py program.rtl [precondition.pkl]
#
# where program.rtl is a text file containing the RTL program
# and precondition.pkl is an optional file containing the precondition to the program
# By default, the program uses config.pkl as precondition.
#
# As output the program repeats its input without comments
# Furthermore, it gives an analysis of how it would run on the
# basic architecture and on the extended architecture.
# Lastly, it gives the postcondition after running the RTL program
# on the indicated precondition.

import sys
import pickle

from antlr4 import *
from RTLLexer import RTLLexer
from RTLParser import RTLParser
from RTLVisitor import RTLVisitor

from RegisterExpression import *
from PrintRTLProgram import PrintVisitor
from VisitorComputeRTLProgram import ComputeVisitor
from VisitorMapRTLOnArchitectureA import CheckVisitorA
from VisitorMapRTLOnArchitectureB import CheckVisitorB

def main(argv):      
    # open the input program, default is test.txt 
    if len(argv) > 1:
        input = FileStream(argv[1])
    else:
        input = FileStream('test.rtl')

    # open the precondition, default is config.pkl
    if len(argv) > 2:
        afile = open(argv[2],'rb')
    else:
        afile = open('config.pkl','rb')
    precondition = pickle.load(afile)
    afile.close()

    # Parse the input program
    lexer = RTLLexer(input)
    stream = CommonTokenStream(lexer)
    parser = RTLParser(stream)
    tree = parser.program()

    # Print the precondition
    print('-------------- PRECONDITION -------------')
    for reg in precondition:
        print(reg + " : " + precondition[reg].getText())

    print('-------------- PROGRAM -------------')
    # Print the program, stripped from comments
    printer = PrintVisitor()
    printer.visit(tree)

    print('-------------- ANALYSIS -------------')
    # Check the program for the basic architecture
    check = CheckVisitorA()
    if check.visit(tree):
        print('Program is correct for the basic architecture \n')
    else:
        print('Program is faulty for the basic architecture \n')

    # Check the program for the extended architecture 
    check = CheckVisitorB()
    if check.visit(tree):
        print('Program is correct for the extended architecture \n')
    else:
        print('Program is faulty for the extended architecture \n')    

    # Calculate the postcondition from the precondition
    compute = ComputeVisitor(precondition)
    postcondition = compute.visit(tree)

    # Print the postcondition
    print('-------------- POSTCONDITION -------------')
    for reg in postcondition:
        print(reg + " : " + postcondition[reg].getText())

    # AFTER THIS LINE, THE PROGRAM DOES NOT NEED TESTING YET
    # THIS PART IS INTENDED FOR AUTOMATIC CHECKING POSTCONDITIONS
    # OF ASSIGNMENTS

    # write the postcondition to file, default is test.pkl
    #if len(argv) > 3:
    #    afile = open(argv[3],'rb')
    #else:
    #    afile = open('test.pkl','wb')
    #pickle.dump(postcondition,afile)
    #afile.close()

    # load the solution, default is the precondition config.pkl
    # the result of loading the default is that you see the
    # difference between precondition and postcondition
    #if len(argv) > 4:
    #    afile = open(argv[3],'rb')
    #else:
    #    afile = open('config.pkl','rb')
    #solution = pickle.load(afile)
    #afile.close()

    # print the difference between postcondition and solution
    #for reg in postcondition:
    #    if reg not in solution:
    #        print(reg + " : " + postcondition[reg].getText() + " was unexpected")
    #    if reg in solution:
    #        if not equal(postcondition[reg],solution[reg]):
    #            print(reg + " : " + postcondition[reg].getText() + " (in stead of " + solution[reg].getText() +")")
    #for reg in solution:
    #    if reg not in postcondition:
    #        if not (type(solution[reg]) is DontCare):
    #            print(reg + " : " + solution[reg].getText() + " is missing")

if __name__ == '__main__':
    main(sys.argv)
